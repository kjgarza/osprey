# Build the Vue app using Node.js
FROM node:18

WORKDIR /app

# Copy the package.json and package-lock.json (if available)
COPY package*.json ./

# Install dependencies
RUN npm install

# Copy the entire project
COPY . .

# Build the app
RUN npm install
RUN npm run build

EXPOSE 5173


CMD ["npm", "run", "host"]