import { createRouter, createWebHistory } from 'vue-router';

// Import your components
import App from './App.vue';
// import DoiForm from '@/components/DoiForm/DoiForm.vue'
// import DefaultLayout from '@/layouts/DefaultLayout.vue'
// import FormLayout from '@/layouts/FormLayout.vue'
import Home from '@/pages/Home.vue';
import MetadataForm from '@/pages/MetadataForm.vue';
import TokenAcquisition from '@/pages/TokenAcquisition.vue';
import UserAccount from '@/pages/UserAccount.vue';

const routes = [
  { path: '/', component: Home },
  { path: '/form', component: MetadataForm },
  { path: '/tokens', component: TokenAcquisition },
  { path: '/account', component: UserAccount },
  // { path: '/layout', component: DefaultLayout },
  // { path: '/doi', component: DoiForm },
  // add more routes as you see fit
];

const router = createRouter({
  history: createWebHistory(),
  routes: routes,
});

// router.beforeEach((to, from, next) => {
//   if (to.matched.some(record => record.meta.requiresAuth) && !auth.isLoggedIn()) {
//     next('/login');
//   } else {
//     next();
//   }
// });

export default router;
