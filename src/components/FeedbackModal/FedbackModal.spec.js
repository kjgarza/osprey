import { shallowMount } from '@vue/test-utils';
import FeedbackModal from '@/components/common/FeedbackModal.vue';

describe('FeedbackModal.vue', () => {
  it('opens modal on trigger', async () => {
    const wrapper = shallowMount(FeedbackModal);
    await wrapper.vm.openModal(); // Simulate modal opening
    expect(wrapper.isVisible()).toBe(true);
  });

  it('emits feedback data on submit', async () => {
    const wrapper = shallowMount(FeedbackModal);
    // Set feedback input value
    wrapper.find('.feedback-input').setValue('Great experience!');
    // Trigger feedback submission
    await wrapper.find('.submit-feedback-button').trigger('click');
    expect(wrapper.emitted().submit).toBeTruthy();
    // Add assertion to check emitted feedback data
  });

  // Additional tests could include validation, modal closing behavior, etc.
});
