import App from './App.vue'
import './index.css'
import '@emdgroup-liquid/liquid/dist/css/liquid.global.css'
import { defineCustomElements } from '@emdgroup-liquid/liquid/dist/loader'
import { createApp } from 'vue'
import router from '@/router';

/* import the fontawesome core */
import { library } from '@fortawesome/fontawesome-svg-core'

/* import font awesome icon component */
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

/* import specific icons */
import { faWandSparkles } from '@fortawesome/free-solid-svg-icons'

library.add(faWandSparkles)



export { session }



declare global {
  interface Window {
    __LD_ASSET_PATH__?: string
  }
}

window.__LD_ASSET_PATH__ = window.location.origin
defineCustomElements()

// createApp(App).mount('#app')
console.log(router.getRoutes());


const app = createApp(App);
app.use(router);
app.component('font-awesome-icon', FontAwesomeIcon);
app.mount('#app');